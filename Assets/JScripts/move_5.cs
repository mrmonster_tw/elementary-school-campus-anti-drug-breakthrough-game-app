﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class move_5 : MonoBehaviour
{
    public bool gameover;
    public int score;
    public Text score_txt;

    public GameObject candy;
    public GameObject jelly;
    public GameObject noodle;
    public GameObject clear;
    GameObject drags_cn;

    public float time;
    public float ran_time;

    public GameObject gm;

    public int check = 0;

    // Start is called before the first frame update
    void Start()
    {
        ran_time = Random.Range(0,3f);
    }

    // Update is called once per frame
    void Update()
    {
        score_txt.text = score.ToString();
        if (score >= 10)
        {
            gameover = true;
            clear.SetActive(true);
            StartCoroutine(back_app_fn());
        }

        if (gameover == false)
        {
            /*if (this.transform.position.x > -11f && Input.GetKey(KeyCode.LeftArrow))
            {
                this.transform.Translate(Vector3.left * 20f * Time.deltaTime);
            }

            if (this.transform.position.x < 11f && Input.GetKey(KeyCode.RightArrow))
            {
                this.transform.Translate(Vector3.right * 20f * Time.deltaTime);
            }*/

            if (Input.GetMouseButton(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.tag == "bear")
                    {
                        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 100);
                        hit.transform.position = Camera.main.ScreenToWorldPoint(mousePosition);

                        hit.transform.position = new Vector3(Camera.main.ScreenToWorldPoint(mousePosition).x,-5.02f,0f);
                    }
                }
            }

            time = time + 1f * Time.deltaTime;

            if (time >= ran_time)
            {
                drag_ins(Random.Range(1,4));
                drag_ins_pos(Random.Range(1, 8));
                ran_time = Random.Range(0, 3f);
                time = 0;
            }
        }
       
    }
    private IEnumerator back_app_fn()
    {
        yield return new WaitForSeconds(3f);
        if (check == 0)
        {
            gm.GetComponent<GM>().level_1.SetActive(true);
        }
        if (check == 1)
        {
            gm.GetComponent<GM>().level_2.SetActive(true);
        }
        clear.SetActive(false);
        gameover = false;
        time = 0;
        ran_time = 0;
        score = 0;
        check++;
        gm.GetComponent<GM>().hint_game_close_fn();
    }
    void drag_ins(int xx)
    {
        if (xx == 1)
        {
            drags_cn = Instantiate(candy);
            Destroy(drags_cn,5f);
        }
        if (xx == 2)
        {
            drags_cn = Instantiate(jelly);
            Destroy(drags_cn, 5f);
        }
        if (xx == 3)
        {
            drags_cn = Instantiate(noodle);
            Destroy(drags_cn, 5f);
        }
    }
    void drag_ins_pos(int xx)
    {
        if (xx == 1)
        {
            drags_cn.transform.localPosition = new Vector3(-10.61f,11.13f,0);
        }
        if (xx == 2)
        {
            drags_cn.transform.localPosition = new Vector3(-7.11f, 11.13f, 0);
        }
        if (xx == 3)
        {
            drags_cn.transform.localPosition = new Vector3(-3.61f, 11.13f, 0);
        }
        if (xx == 4)
        {
            drags_cn.transform.localPosition = new Vector3(-0.11f, 11.13f, 0);
        }
        if (xx == 5)
        {
            drags_cn.transform.localPosition = new Vector3(3.39f, 11.13f, 0);
        }
        if (xx == 6)
        {
            drags_cn.transform.localPosition = new Vector3(6.89f, 11.13f, 0);
        }
        if (xx == 7)
        {
            drags_cn.transform.localPosition = new Vector3(10.39f, 11.13f, 0);
        }
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "drag")
        {
            print("des");
            score++;
            Destroy(col.transform.gameObject);
        }
    }
}
