﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class recover_image : MonoBehaviour
{
    public Sprite org;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void go_fn()
    {
        StartCoroutine(recover_fn());
    }
    private IEnumerator recover_fn()
    {
        yield return new WaitForSeconds(2f);
        this.GetComponent<Image>().sprite = org;
    }
}
