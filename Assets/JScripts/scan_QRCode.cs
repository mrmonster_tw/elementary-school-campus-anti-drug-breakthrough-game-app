﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using ZXing;

public class scan_QRCode : MonoBehaviour
{
    private WebCamTexture myCam;//接收攝影機返回的圖片數據
    private BarcodeReader reader = new BarcodeReader();//ZXing的解碼
    private Result res;//儲存掃描後回傳的資訊
    private bool flag = true;//判斷掃描是否執行完畢

    public RawImage background;
    public int check = 0;

    public GameObject gm;
    public GameObject scanobs;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        background.texture = myCam;
        if (myCam != null)//若有攝影機則將攝影機拍到的畫面畫出
        {
            if (myCam.isPlaying == true && check == 0)//若攝影機已開啟
            {
                StartCoroutine(scan());
            }
        }
    }
    public void opencam_fn()
    {
        StartCoroutine(open_Camera());//開啟攝影機鏡頭
        check = 0;
    }

    private IEnumerator open_Camera()
    {
        yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);    //授權開啟鏡頭
        if (Application.HasUserAuthorization(UserAuthorization.WebCam))
        {
            //設置攝影機要攝影的區域
            myCam = new WebCamTexture(WebCamTexture.devices[0].name, Screen.width, Screen.height, 60);/* (攝影機名稱, 攝影機要拍到的寬度, 攝影機要拍到的高度, 攝影機的FPS) */
            myCam.Play();//開啟攝影機
        }
    }
    private IEnumerator scan()
    {
        yield return new WaitForSeconds(2);
        //Texture2D t2D = new Texture2D(Screen.width, Screen.height);//掃描後的影像儲存大小，越大會造成效能消耗越大，若影像嚴重延遲，請降低儲存大小。
        Texture2D t2D = new Texture2D(300, 300);//掃描後的影像儲存大小，越大會造成效能消耗越大，若影像嚴重延遲，請降低儲存大小。
        yield return new WaitForEndOfFrame();//等待攝影機的影像繪製完畢

        //t2D.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);//掃描的範圍，設定為整個攝影機拍到的影像，若影像嚴重延遲，請降低掃描大小。
        t2D.ReadPixels(new Rect(480, 240, 400, 400), 0, 0, false);
        t2D.Apply();//開始掃描

        res = reader.Decode(t2D.GetPixels32(), t2D.width, t2D.height);//對剛剛掃描到的影像進行解碼，並將解碼後的資料回傳

        //若是掃描不到訊息，則res為null
        if (res != null)
        {
            Debug.Log(res.Text);//將解碼後的資料列印出來
            if (res.Text == "level_1" && gm.GetComponent<GM>().stage == 1)
            {
                scanobs.SetActive(false);
                gm.GetComponent<GM>().level_1_ques.SetActive(true);
                myCam.Stop();
                check = 1;
            }
            if (res.Text == "level_2" && gm.GetComponent<GM>().stage == 2)
            {
                scanobs.SetActive(false);
                gm.GetComponent<GM>().level_2_ques.SetActive(true);
                myCam.Stop();
                check = 1;
            }
            if (res.Text == "level_3" && gm.GetComponent<GM>().stage == 3)
            {
                scanobs.SetActive(false);
                gm.GetComponent<GM>().level_3_front.SetActive(true);
                myCam.Stop();
                check = 1;
            }
            if (res.Text == "level_3_movie" && gm.GetComponent<GM>().stage == 4)
            {
                scanobs.SetActive(false);
                gm.GetComponent<GM>().start_play();
                myCam.Stop();
                check = 1;
            }
            
        }
    }
    public void closecam_fn()
    {
        if (gm.GetComponent<GM>().stage == 1)
        {
            scanobs.SetActive(false);
            gm.GetComponent<GM>().level_1.SetActive(true);
            myCam.Stop();
            check = 0;
        }
        if (gm.GetComponent<GM>().stage == 2)
        {
            scanobs.SetActive(false);
            gm.GetComponent<GM>().level_2.SetActive(true);
            myCam.Stop();
            check = 0;
        }
        if (gm.GetComponent<GM>().stage == 3)
        {
            scanobs.SetActive(false);
            gm.GetComponent<GM>().level_2_互動.SetActive(true);
            myCam.Stop();
            check = 0;
        }
        if (gm.GetComponent<GM>().stage == 4)
        {
            scanobs.SetActive(false);
            gm.GetComponent<GM>().level_3.SetActive(true);
            myCam.Stop();
            check = 0;
        }
    }
}
