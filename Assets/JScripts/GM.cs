﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class GM : MonoBehaviour
{
    public int stage;
    public RectTransform word_1;
    public GameObject event_set;
    float he_d;
    public bool timecount;
    public int t_time_min;
    public float t_time_sec;
    public GameObject time_txt;
    public Text time_min_txt;
    public Text time_sec_txt;
    public Text clear_min_txt;
    public Text symbol;
    public Text clear_sec_txt;
    public Text clearpage_min_txt;
    public Text clearpage_sec_txt;
    public GameObject frontpage;
    public GameObject frontpage_1;
    public GameObject frontpage_login;
    public GameObject frontpage_bg;

    public bool movie_play;
    public GameObject movie;
    public VideoClip LV3_movie;
    public int movie_check = 0;

    public GameObject scanobs;
    public GameObject cam;
    public GameObject score_txt;

    public GameObject descript;
    public GameObject descript_play;

    public GameObject hint_frontpage;
    public GameObject hint_game;
    public GameObject hint_1_bt;
    public GameObject hint_pic_1;
    public GameObject hint_2_bt;
    public GameObject hint_pic_2;

    public GameObject level_1_front;
    public GameObject level_1;
    public GameObject level_1_ques;
    public int ques_num = 1;
    public Text ques_num_Text;

    public GameObject level_2_front;
    public GameObject level_2;
    public GameObject level_2_ques;
    public GameObject level_2_互動;

    public GameObject level_3_front;
    public GameObject level_3;
    public GameObject level_3_ques;

    public GameObject clear;
    public GameObject clear_page;
    public GameObject right;
    public GameObject wrong;

    // Start is called before the first frame update
    void Start()
    {
        //首頁
        StartCoroutine(frontpage_fn());
    }

    // Update is called once per frame
    void Update()
    {
        //說明
        if (he_d < 480f && descript.activeSelf)
        {
            he_d = he_d + 30F * Time.deltaTime;
            word_1.sizeDelta = new Vector2(1320, he_d);
        }
        if (he_d >= 480f)
        {
            descript_play.SetActive(true);
        }

        if (timecount)
        {
            if (t_time_sec < 60)
            {
                t_time_sec = t_time_sec + 1f * Time.deltaTime;
            }
            if (t_time_sec >= 60)
            {
                t_time_sec = 0;
                t_time_min++;
            }
            time_min_txt.text = t_time_min.ToString();
            time_sec_txt.text = Mathf.Floor(t_time_sec).ToString();
        }
        

        if (movie.GetComponent<VideoPlayer>().isPlaying == false && movie_play)
        {
            if (movie_check == 1)
            {
                print("aa");
                level_3_ques.SetActive(true);
                movie.SetActive(false);
                movie_play = false;
            }
            if (movie_check == 0)
            {
                descript.SetActive(true);
                movie.SetActive(false);
                movie.GetComponent<VideoPlayer>().clip = LV3_movie;
                movie_play = false;
                movie_check = 1;
            }
        }
        

        if (ques_num == 4)
        {
            level_1_ques.SetActive(false);
            level_2_front.SetActive(true);
            ques_num++;
        }
    }
    private IEnumerator frontpage_fn()
    {
        yield return new WaitForSeconds(5f);
        frontpage_login.SetActive(true);
        frontpage_bg.GetComponent<rotate>().enabled = false;
    }
    //進入闖關播放影片
    public void start_play()
    {
        frontpage_1.SetActive(false);
        frontpage.SetActive(false);
        frontpage_login.SetActive(false);
        movie.SetActive(true);
        StartCoroutine(movie_start_check());
    }
    public void review_fn()
    {
        level_3_ques.SetActive(false);
        start_play();
    }
    private IEnumerator movie_start_check()
    {
        yield return new WaitForSeconds(2f);
        movie_play = true;
    }
    public void scan_fn()
    {
        cam.GetComponent<scan_QRCode>().opencam_fn();
        scanobs.SetActive(true);
        level_1.SetActive(false);
        level_2_front.SetActive(false);
        level_2.SetActive(false);
        level_2_互動.SetActive(false);
        level_3.SetActive(false);
    }
    public void hint_front_page_lv1()
    {
        level_1.SetActive(false);
        hint_frontpage.SetActive(true);
        hint_1_bt.SetActive(false);
        hint_pic_1.SetActive(true);
    }
    public void hint_front_page_lv2()
    {
        level_2.SetActive(false);
        hint_frontpage.SetActive(true);
        hint_2_bt.SetActive(false);
        hint_pic_2.SetActive(false);
    }
    public void hint_game_fn()
    {
        hint_frontpage.SetActive(false);
        hint_game.SetActive(true);
        score_txt.SetActive(true);
    }
    public void hint_game_close_fn()
    {
        hint_game.SetActive(false);
        score_txt.SetActive(false);
    }
    public void anwer_lv1()
    {
        if (ques_num <= 3)
        {
            right.SetActive(true);
            event_set.SetActive(false);
            StartCoroutine(anwer_hold_1());
        }
    }
    public void anwer_lv2()
    {
        if (ques_num == 5)
        {
            right.SetActive(true);
            event_set.SetActive(false);
            StartCoroutine(anwer_hold_2());
        }
    }
    public void anwer_lv3()
    {
        if (ques_num == 6)
        {
            right.SetActive(true);
            event_set.SetActive(false);
            StartCoroutine(anwer_hold_3());
        }
    }
    private IEnumerator anwer_hold_1()
    {
        yield return new WaitForSeconds(2f);
        ques_num++;
        ques_num_Text.text = ques_num.ToString();
        right.SetActive(false);
        wrong.SetActive(false);
        event_set.SetActive(true);
    }
    private IEnumerator anwer_hold_2()
    {
        yield return new WaitForSeconds(2f);
        ques_num++;
        right.SetActive(false);
        wrong.SetActive(false);
        event_set.SetActive(true);

        level_2_ques.SetActive(false);
        level_2_互動.SetActive(true);
        stage = 3;
    }
    private IEnumerator anwer_hold_3()
    {
        yield return new WaitForSeconds(2f);
        ques_num++;
        right.SetActive(false);
        wrong.SetActive(false);
        event_set.SetActive(true);
        clear_fn();
    }
    public void clear_fn()
    {
        level_3_ques.SetActive(false);
        clear.SetActive(true);
        StartCoroutine(clear_score());

        timecount = false;
    }
    private IEnumerator clear_score()
    {
        yield return new WaitForSeconds(2f);
        clear_min_txt.enabled = true;
        symbol.enabled = true;
        clear_sec_txt.enabled = true;
        clear_min_txt.text = t_time_min.ToString();
        clear_sec_txt.text = Mathf.Floor(t_time_sec).ToString();
        yield return new WaitForSeconds(5f);
        clear.SetActive(false);
        clear_page.SetActive(true);
        clearpage_min_txt.text = t_time_min.ToString();
        clearpage_sec_txt.text = Mathf.Floor(t_time_sec).ToString();
    }
    public void wrong_anwer()
    {
        ques_num++;
    }
    public void LV_1_frontpage()
    {
        level_1_front.SetActive(true);
        descript.SetActive(false);
        time_txt.SetActive(true);

        timecount = true;
    }
    public void LV_1_go()
    {
        level_1_front.SetActive(false);
        level_1.SetActive(true);
        stage = 1;
    }
    public void LV_2_go()
    {
        level_2_front.SetActive(false);
        level_2.SetActive(true);
        stage = 2;
    }
    public void LV_3_go()
    {
        level_3_front.SetActive(false);
        level_3.SetActive(true);
        stage = 4;
    }
    
    public void back_fn(GameObject xx)
    {
        xx.SetActive(true);
        scanobs.SetActive(false);
    }
    public void quit_fn()
    {
        Application.Quit();
    }
    public void restart_fn()
    {
        SceneManager.LoadScene(0);
    }
}
